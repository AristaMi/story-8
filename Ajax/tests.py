from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTestLab8 (TestCase):
	def test_apakah_website_running(self):
		c = Client()
		response = c.get('/')
		self.assertEqual(response.status_code, 200)

	def test_apa_website_menggunakan_Ajax_html(self):
		c = Client()
		response = c.get('/')
		self.assertTemplateUsed(response, "Ajax.html")

	def test_apakah_website_memanggil_fungsi_index_di_views(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_apakah_website_memanggil_fungsi_get_data_di_views(self):
		found = resolve('/getData/<str:key>')
		self.assertEqual(found.func, get_data)

	def test_apa_ada_jquery(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("http://code.jquery.com/jquery-3.4.1.js", content)

	def test_apa_ada_table(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("<table", content)

	def test_apa_ada_button(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("button", content)

	def test_apa_ada_tulisan_submit(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("submit", content)

class FunctionalTestLab8 (LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTestLab8, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTestLab8, self).tearDown()

	def test_cari_buku(self):
		selenium = self.selenium
		# buka link yg mau dites
		selenium.get(self.live_server_url)
		selenium.implicitly_wait(10)
		time.sleep(5)

		# cari elemen
		searchinput = selenium.find_element_by_id('input')
		button = selenium.find_element_by_id('button')
		selenium.implicitly_wait(10)
		time.sleep(5)

		# isi search box
		searchinput.send_keys('Grimmly')
		selenium.implicitly_wait(5)
		time.sleep(5)

		# klik tombol cari buku
		button.send_keys(Keys.RETURN)
