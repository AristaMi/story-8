from django.contrib import admin
from django.urls import path
from . import views

app_name = "Ajax"

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', views.index, name = 'index'),
	path('getData/<str:key>', views.get_data),
]