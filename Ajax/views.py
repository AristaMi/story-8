from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
# UNTUK SEMENTARA DEFAULT, NANTI EDIT LAGI
def index(request):
	return render (request, "Ajax.html")

def get_data(request, key):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
	response = requests.get(url)
	response_json = response.json()

	return JsonResponse(response_json)