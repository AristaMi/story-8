$(document).ready(function() {
	$('#button').click(function() {
		$('#result').empty();
		var key = $('#input').val();

		$.ajax({
			method	: 'GET',
			url			: 'http://localhost:8000/getData/' + key,
			// url			: 'story-8-ppw.herokuapp.com/getData/' + key,
			success : function(response) {
									console.log(response);

									for (let i=0; i < response.items.length; i++) {
										$('#result').append('<tr>');
										var sampul = response.items[i].volumeInfo.imageLinks;

										if (typeof sampul != 'undefined') {
											sampul = sampul.thumbnail;
											$('#result').append('<td><img src="' + sampul + '"></td>');
										}

										else {
											$('#result').append('<td>No image available</td>');
										}

										var judul = response.items[i].volumeInfo.title;
										$('#result').append('<td>' + judul + '</td>');

										var penulis = response.items[i].volumeInfo.penulis;
										$('#result').append('<td>' + penulis + '</td>');
										$('#result').append('</tr>');
									}
								}
		})
	});
})